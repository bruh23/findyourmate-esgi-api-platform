<?php

namespace App\Controller;


use App\Repository\UserRepository;
use App\Service\GenerateTokenService;
use App\Service\NotifyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RessetPasswordRequest extends AbstractController
{
    private $entityManager;
    private $userRepository;
    private $notify;
    private $genetateToken;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, NotifyService $notify, GenerateTokenService $generateToken)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->notify = $notify;
        $this->genetateToken = $generateToken;

    }

    public function __invoke($data)
    {

        $email=$data->getEmail();

        $user = $this->checkEmailExist($email);
        if (!$user) {
            throw  new BadRequestHttpException("Ce compte n'existe pas");

        }


        $token = $this->genetateToken->generateToken();

        $user->setResetPasswordToken($token);
        $user->setResetPasswordTokenCreatedAt(new \DateTime());
        $this->notify->sendMail($user->getEmail(), "FindYourMate : Reset Password", $token, $user, "reset.password.html.twig");



        $this->entityManager->persist($user);
        $this->entityManager->flush();

       // dd($user);
        $data = [
            'email' => $user->getEmail(),
            'message' => 'un mail de réinitialisation vient d\'etre envoyé'
        ];

        return $this->json($data, $status = 200);

    }

    private function checkEmailExist($email)
    {
        $user = $this->userRepository->findOneByEmail($email);
        if (!$user) {
            return;
        }
        return $user;
    }

}
