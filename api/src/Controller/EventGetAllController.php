<?php


namespace App\Controller;


use App\Repository\BlockRepository;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventGetAllController  extends AbstractController
{

    private $entityManager;
    private $eventRepository;
    private $blockRepository;

    public function __construct(EntityManagerInterface $entityManager, EventRepository $eventRepository, BlockRepository $blockRepository)
    {
        $this->entityManager = $entityManager;
        $this->eventRepository = $eventRepository;
        $this->blockRepository = $blockRepository;
    }


    public function __invoke($data)
    {

        $user = $this->getUser();
        $blocks = $this->blockRepository->findBlockedsByUser($user);
        array_push($blocks, $user->getId());
        $events = $this->eventRepository->findEventsNotInBlocked($blocks);

        return $events;

    }


}
