<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ConfirmAccountUserController extends AbstractController
{

    private $entityManager;
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }


    public function __invoke($id, $token)
    {

        $user = $this->userRepository->findOneById($id);

        if (!$user || $user->getTokenEmail() !== $token) {
            throw  new BadRequestHttpException("Token invalide Morata");

        }

        $user->setValidateMail(true);
        $user->setvalidatedAt(new \DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();


        $data = [
            'email' => $user->getEmail(),
            'validateMail' => true
        ];

        return $this->json($data, $status = 200);

    }

    private function checkEmailExist($email)
    {
        $user = $this->userRepository->findOneByEmail($email);
        if (!$user) {
            return;
        }
        return $user;
    }
}
