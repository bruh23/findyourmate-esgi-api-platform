<?php


namespace App\Controller;


use App\Repository\UserRepository;
use App\Service\EncodePasswordService;
use App\Service\GenerateTokenService;
use App\Service\NotifyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RessetPasswordResponse extends AbstractController
{
    private $entityManager;
    private $userRepository;
    private $notify;
    private $genetateToken;
    private $encoderPassword;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, NotifyService $notify, GenerateTokenService $generateToken, EncodePasswordService $encoderPassword)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->notify = $notify;
        $this->genetateToken = $generateToken;
        $this->encoderPassword = $encoderPassword;

    }

    public function __invoke($id, $token,$data)
    {

        $user = $this->userRepository->findOneById($id);

        if (!$user || $user->getResetPasswordToken() !== $token) {
            throw  new BadRequestHttpException("Token invalide password response");

        }
        $newPassword = $data->getPassword();
        $user->setPassword($this->encoderPassword->encodePassword($user,$newPassword));
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->notify->sendMail($user->getEmail(), "Mot de passe changé - FindYourMate ", null, $user, "password.changed.html.twig");

        $data = [
            'email' => $user->getEmail(),
            'message' => ''
        ];


        return $this->json($data, $status = 200);

    }

}
