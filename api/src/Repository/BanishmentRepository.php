<?php

namespace App\Repository;

use App\Entity\Banishment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Banishment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Banishment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Banishment[]    findAll()
 * @method Banishment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BanishmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Banishment::class);
    }

    // /**
    //  * @return Banishment[] Returns an array of Banishment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Banishment
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
