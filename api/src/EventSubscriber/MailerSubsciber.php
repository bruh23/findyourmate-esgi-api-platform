<?php


namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Service\GenerateTokenService;
use App\Service\NotifyService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\MailerInterface;


class MailerSubsciber implements EventSubscriberInterface
{

    private $mailer;
    private $notify;

    public function __construct(MailerInterface $mailer, NotifyService $notify, GenerateTokenService $generateToken)
    {
        $this->mailer = $mailer;
        $this->notify = $notify;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::POST_WRITE],
        ];
    }

    public function sendMail(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $this->notify->sendMail($user->getEmail(), "FindYourMate : Confimation de compte", $user->getTokenEmail(), $user, "validation.account.user.html.twig");


    }

}
