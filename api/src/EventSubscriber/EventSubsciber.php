<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Event;
use App\Entity\User;
use App\Service\NotifyService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class EventSubsciber implements EventSubscriberInterface
{

    private array $methodNotAllowed = [


    ];

    private Security $security ;
    private $notify;

    public function __construct(Security $security,NotifyService $notify)
    {
        $this->security =$security;
        $this->notify = $notify;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['checkEvent', EventPriorities::PRE_VALIDATE],

        ];
    }

    public function checkEvent(ViewEvent $event)
    {
        $eventNew = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($eventNew instanceof Event) {
            switch ($method) {
                case Request::METHOD_POST:

                    $eventNew->setOrganizer($this->security->getUser());
                    $this->notify->sendMail($eventNew->getOrganizer()->getEmail(), " Un nouveau Evenet , FindYourMate", null, $eventNew->getOrganizer(), "create.new.event.html.twig");

                    break;

                case Request::METHOD_DELETE:
                case Request::METHOD_PATCH:
                case Request::METHOD_GET:
                    break;

            }
        }else{
            return;
        }





    }
}
