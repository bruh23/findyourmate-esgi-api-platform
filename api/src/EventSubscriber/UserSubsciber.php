<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\GenerateTokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class UserSubsciber implements EventSubscriberInterface
{

    private array $methodNotAllowed = [
        Request::METHOD_GET,
    ];

    private $entityManager;
    private $genetateToken;

    public function __construct(UserRepository $userRepository, GenerateTokenService $generateToken)
    {

        $this->userRepository = $userRepository;
        $this->genetateToken = $generateToken;

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['checkUser', EventPriorities::PRE_VALIDATE],

        ];
    }

    public function checkUser(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || in_array($method, $this->methodNotAllowed, true)) {
            return;
        } else {
            switch ($method) {
                case Request::METHOD_POST:

                    if ($this->checkEmailExist($user->getEmail())) {
                        throw  new BadRequestHttpException("Cette adresse mail existe déja");
                    }

                    $user->setRoles(["ROLE_USER"]);
                    $user->setOwner($user);
                    $user->setTokenEmail($this->genetateToken->generateToken());
                    $user->setTokenEmailCreatedAt(new \DateTime());

                    break;

                case Request::METHOD_PATCH:
                case Request::METHOD_DELETE:
                case Request::METHOD_GET:
                    break;

            }
        }


    }


    /**
     * @param $email
     */
    private function checkEmailExist($email)
    {
        $user = $this->userRepository->findOneByEmail($email);
        if (!$user) {
            return;
        }
        return $user;
    }

}
