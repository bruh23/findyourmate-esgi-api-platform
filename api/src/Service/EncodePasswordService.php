<?php


namespace App\Service;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EncodePasswordService
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct( UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function encodePassword($user,$password)
    {


            $passwordHach = $this->encoder->encodePassword($user,$password);
            $user->setPassword($passwordHach);
        return $passwordHach;
    }
}
