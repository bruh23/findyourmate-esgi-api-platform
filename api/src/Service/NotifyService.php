<?php

namespace App\Service;


use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Mailer\MailerInterface;

class NotifyService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }
    public function sendMail($to, $object, $token=null, $entity, $view): void
    {

        $message = (new TemplatedEmail())
            ->from('findyourmate1@gmail.com')
            ->to('findyourmate1@gmail.com') //$to
            ->subject("FindYourMate")
//            ->text('Votre compte a été créé !')
            ->htmlTemplate('email/' . $view)
//            ->htmlTemplate('email/validation.account.user.html.twig')
            ->context([
                'expiration_date' => new \DateTime('+1 days'),
                'user' => $entity,
                'token' => $token
            ])
        ;

        $this->mailer->send($message);


    }
}
