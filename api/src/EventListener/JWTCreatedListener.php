<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Security;

class JWTCreatedListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    private $security;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, Security $security)
    {
        $this->requestStack = $requestStack;
        $this->security = $security;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        $user = $this->security->getUser();

        $payload = $event->getData();

        if ($user) {
            $payload['id'] = $user->getId();
            $payload['firstName'] = $user->getFirstName();
            $payload['lastName'] = $user->getLastname();
            $payload['gender'] = $user->getGender();
            $payload['birthday'] = $user->getBirthday();
            $payload['address'] = $user->getAddress();
            $payload['postalCode'] = $user->getPostalCode();
            $payload['additionalAddress'] = $user->getAdditionalAddress();
            $payload['discordName'] = $user->getDiscordName();
            $payload['createdAt'] = $user->getCreatedAt();
            $payload['updatedAt'] = $user->getUpdatedAt();
            $payload['blockAdmin'] = $user->getBlockAdmin();
            $payload['blockDuration'] = $user->getBlockDuration();
            $payload['blockedAt'] = $user->getBlockedAt();
            $payload['blockWhy'] = $user->getBlockWhy();
            $payload['validateMail'] = $user->getValidateMail();
            $payload['picture'] = $user->getPicture();
        }


        $event->setData($payload);

        $header = $event->getHeader();
        $header['cty'] = 'JWT';

        $event->setHeader($header);
    }
}
