<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SignalementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=SignalementRepository::class)
 * @ApiResource(
 *
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"signalement_post_norm","signalement_post_denorm","signalement_get_norm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *          "post"={
 *              "normalization_context"={"groups"={"signalement_post_norm","signalement_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          }
 *     },
 *
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"signalement_post_norm","signalement_post_denorm","signalement_get_norm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *     }
 * )
 */
class Signalement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"signalement_get_norm"})
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"signalement_post_norm"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"signalement_get_norm"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"signalement_post_denorm","signalement_post_norm"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="signalements")
     * @Groups({"signalement_post_denormsignalement_post_denorm","signalement_post_norm"})
     */
    private $user_signalement;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="signaled")
     * @Groups({"signalement_post_denorm","signalement_post_norm"})
     */
    private $user_signaledd;

    /**
     * @ORM\ManyToOne(targetEntity=Banishment::class, inversedBy="signalements")
     * @Groups({"signalement_post_denorm","signalement_post_norm"})
     */
    private $banishmentType;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserSignalement(): ?User
    {
        return $this->user_signalement;
    }

    public function setUserSignalement(?User $user_signalement): self
    {
        $this->user_signalement = $user_signalement;

        return $this;
    }

    public function getUserSignaledd(): ?User
    {
        return $this->user_signaledd;
    }

    public function setUserSignaledd(?User $user_signaledd): self
    {
        $this->user_signaledd = $user_signaledd;

        return $this;
    }

    public function getBanishmentType(): ?Banishment
    {
        return $this->banishmentType;
    }

    public function setBanishmentType(?Banishment $banishmentType): self
    {
        $this->banishmentType = $banishmentType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


}
