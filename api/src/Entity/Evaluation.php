<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EvaluationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EvaluationRepository::class)
 * @ApiResource(
 *  collectionOperations={
 *          "get",
 *          "post"
 *     },
 *
 *     itemOperations={
 *          "get",
 *          "delete",
 *          "patch"
 *     }
 *     )
 */
class Evaluation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"event_read_norm"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"event_read_norm"})
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"event_read_norm"})
     */
    private $comment;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"event_read_norm"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=EventRegistration::class, inversedBy="evaluations")
     * @ORM\JoinColumn(nullable=false, onDelete="SET NULL")
     */
    private $eventRegistration;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="evaluationsOthers")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"event_read_norm"})
     */
    private $evaluator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getEventRegistration(): ?EventRegistration
    {
        return $this->eventRegistration;
    }

    public function setEventRegistration(?EventRegistration $eventRegistration): self
    {
        $this->eventRegistration = $eventRegistration;

        return $this;
    }

    public function getEvaluator(): ?User
    {
        return $this->evaluator;
    }

    public function setEvaluator(?User $evaluator): self
    {
        $this->evaluator = $evaluator;

        return $this;
    }
}
