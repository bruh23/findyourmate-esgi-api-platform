<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ApiResource(
 *
 *  attributes={
 *     "force_eager"=false },
 *
 *
 *  collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"user_get_no","user_get_no_1"},"enable_max_depth"=true},
 *              "security"="is_granted('ROLE_USER')","security_message"="Vous n'etes pas ADMIN ! Une connexion à votre compte FindYourMate est obligatoire"
 *           },
 *         "post"={
 *              "normalization_context"={"groups"={"user_post_norm"}},
 *              "denormalization_context"={"groups"={"user_post_deno"}},
 *          },
 *          "password_reset_request": {
 *             "method": "post",
 *             "path": "/reset-password/request",
 *             "controller": App\Controller\RessetPasswordRequest::class,
 *             "denormalization_context"={"groups"={"user_post_email"}},
 *          }
 *     },
 *
 *  itemOperations={
 *
 *          "get"={
 *              "normalization_context"={"groups"={"user_get_no"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *           },
 *
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getOwner() == user)", "security_message"="Vous ne pouvez pas supprimer le compte, vous n'êtes pas le propriétaire de ce compte."
 *          },
 *
 *          "patch"={
 *              "normalization_context"={"groups"={"user_patch_deno","user_patch_no"}},
 *              "denormalization_context"={"groups"={"user_post_deno","user_patch_deno"}},
 *              "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getOwner() == user)", "security_message"="Update interdit  Vous n'êtes pas le propriétaire de ce compte."
 *          },
 *          "confirmation_account_user": {
 *             "method": "get",
 *             "path": "/confirmation-account/{id}/{token}",
 *             "controller": App\Controller\ConfirmAccountUserController::class,
 *             "read"=false,
 *             "output"=false,
 *         },
 *
 *          "password_reset_response": {
 *             "method": "patch",
 *             "path": "/reset-password/response/{id}/{token}",
 *             "controller": App\Controller\RessetPasswordResponse::class,
 *             "denormalization_context"={"groups"={"user_patch_no_password"}},

 *         }
 *
 *    }
 * )
 *
 */
class User implements UserInterface
{

    const GENDER = ['homme' => 'm', 'femme' => 'f', 'autre' => 'au'];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"event_read_norm","evaluation_read_n","user_get_no"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"event_read_norm","evaluation_read_n","user_get_no","user_post_deno","user_post_norm","user_patch_no","user_post_email"})
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user_post_deno","user_patch_no_password","user_get_no_1"})
     */
    private $password;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     * @Groups({""})
     */
    private $resetPasswordToken;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({""})
     */
    private $resetPasswordTokenCreatedAt;

    /**
     * @var string The hashed password
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({""})
     */
    private $resetPasswordTokenConfirmation;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     * @Groups({"event_read_norm","evaluation_read_n","user_get_no","user_patch_deno"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","evaluation_read_n","user_get_no","user_patch_deno"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(choices=User::GENDER, groups={"complete"})
     * @Groups({"event_read_norm","evaluation_read_n","user_get_no","user_patch_deno"})
     */
    private $gender;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $additionalAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","user_get_no","user_patch_deno"})
     */
    private $discordName;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"user_get_no","user_post_norm"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"user_get_no","user_patch_no"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $blockAdmin;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $blockDuration;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $blockedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $blockWhy;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $validateMail;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $validatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_patch_deno","user_token_validate_deno"})
     */
    private $tokenEmail;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_patch_deno"})
     */
    private $tokenEmailCreatedAt;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get_no","user_patch_deno"})
     */
    private $picture;


    /**
     * @ORM\OneToMany(targetEntity=Block::class, mappedBy="user_blocker" , cascade={"persist", "remove"})
     * @Groups({"user_get_no"})
     */
    private $blocks;

    /**
     * @ORM\OneToMany(targetEntity=Signalement::class, mappedBy="user_signalement", cascade={"persist", "remove"})
     */
    private $signalements;

    /**
     * @ORM\OneToMany(targetEntity=Signalement::class, mappedBy="user_signaledd", cascade={"persist", "remove"})
     */
    private $signaled;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="organizer", cascade={"persist", "remove"})
     * @Groups({"user_get_no"})
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=EventRegistration::class, mappedBy="participant")
     * @Groups({"user_get_no"})
     */
    private $eventRegistrations;

    /**
     * @ORM\OneToMany(targetEntity=Evaluation::class, mappedBy="evaluator")
     * @Groups({"user_get_no"})
     */
    private $evaluationsOthers;

    /**
     * @ORM\ManyToMany(targetEntity=Badge::class, inversedBy="users")
     * @Groups({"user_get_no"})
     */
    private $badges;

    /**
     * @ORM\OneToOne(targetEntity=Certification::class, inversedBy="userCertification", cascade={"persist", "remove"})
     * @Groups({"user_get_no"})
     */
    private $certification;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Block::class, mappedBy="user_blocked" , cascade={"persist", "remove"})
     */
    private $blockeds;

    public function __construct()
    {
        $this->blocks = new ArrayCollection();
        $this->signalements = new ArrayCollection();
        $this->signaled = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->eventRegistrations = new ArrayCollection();
        $this->evaluationsOthers = new ArrayCollection();
        $this->badges = new ArrayCollection();
        $this->blockeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getResetPasswordToken(): string
    {
        return $this->resetPasswordToken;
    }

    /**
     * @param string $resetPasswordToken
     */
    public function setResetPasswordToken(string $resetPasswordToken): void
    {
        $this->resetPasswordToken = $resetPasswordToken;
    }

    /**
     * @return mixed
     */
    public function getResetPasswordTokenCreatedAt()
    {
        return $this->resetPasswordTokenCreatedAt;
    }

    /**
     * @param mixed $resetPasswordTokenCreatedAt
     */
    public function setResetPasswordTokenCreatedAt($resetPasswordTokenCreatedAt): void
    {
        $this->resetPasswordTokenCreatedAt = $resetPasswordTokenCreatedAt;
    }

    /**
     * @return string
     */
    public function getResetPasswordTokenConfirmation(): string
    {
        return $this->resetPasswordTokenConfirmation;
    }

    /**
     * @param string $resetPasswordTokenConfirmation
     */
    public function setResetPasswordTokenConfirmation(string $resetPasswordTokenConfirmation): void
    {
        $this->resetPasswordTokenConfirmation = $resetPasswordTokenConfirmation;
    }



    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday): void
    {
        $this->birthday = $birthday;
    }


    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlockAdmin()
    {
        return $this->blockAdmin;
    }

    /**
     * @param mixed $blockAdmin
     */
    public function setBlockAdmin($blockAdmin): void
    {
        $this->blockAdmin = $blockAdmin;
    }



    public function getValidateMail(): ?bool
    {
        return $this->validateMail;
    }

    public function setValidateMail(bool $validateMail): self
    {
        $this->validateMail = $validateMail;

        return $this;
    }

    public function getvalidatedAt(): ?\DateTimeInterface
    {
        return $this->validatedAt;
    }

    public function setvalidatedAt(\DateTimeInterface $validatedAt): self
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    public function getBlockedAt(): ?\DateTimeInterface
    {
        return $this->blockedAt;
    }

    public function setBlockedAt(\DateTimeInterface $blockedAt): self
    {
        $this->blockedAt = $blockedAt;

        return $this;
    }

    public function getBlockDuration(): ?\DateTimeInterface
    {
        return $this->blockDuration;
    }

    public function setBlockDuration(\DateTimeInterface $blockDuration): self
    {
        $this->blockDuration = $blockDuration;

        return $this;
    }

    public function getTokenEmail(): ?string
    {
        return $this->tokenEmail;
    }

    public function setTokenEmail(string $tokenEmail): self
    {
        $this->tokenEmail = $tokenEmail;

        return $this;
    }

    public function getTokenEmailCreatedAt(): ?\DateTimeInterface
    {
        return $this->tokenEmailCreatedAt;
    }

    public function setTokenEmailCreatedAt(\DateTimeInterface $tokenEmailCreatedAt): self
    {
        $this->tokenEmailCreatedAt = $tokenEmailCreatedAt;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getDiscordName(): ?string
    {
        return $this->discordName;
    }

    public function setDiscordName(string $discordName): self
    {
        $this->discordName = $discordName;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getBlockWhy(): ?string
    {
        return $this->blockWhy;
    }

    public function setBlockWhy(string $blockWhy): self
    {
        $this->blockWhy = $blockWhy;

        return $this;
    }

    /**
     * @return Collection|Block[]
     */
    public function getBlocks(): Collection
    {
        return $this->blocks;
    }

    public function addBlock(Block $block): self
    {
        if (!$this->blocks->contains($block)) {
            $this->blocks[] = $block;
            $block->setUserBlocker($this);
        }

        return $this;
    }

    public function removeBlock(Block $block): self
    {
        if ($this->blocks->contains($block)) {
            $this->blocks->removeElement($block);
            // set the owning side to null (unless already changed)
            if ($block->getUserBlocker() === $this) {
                $block->setUserBlocker(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Signalement[]
     */
    public function getSignalements(): Collection
    {
        return $this->signalements;
    }

    public function addSignalement(Signalement $signalement): self
    {
        if (!$this->signalements->contains($signalement)) {
            $this->signalements[] = $signalement;
            $signalement->setUserSignalement($this);
        }

        return $this;
    }

    public function removeSignalement(Signalement $signalement): self
    {
        if ($this->signalements->contains($signalement)) {
            $this->signalements->removeElement($signalement);
            // set the owning side to null (unless already changed)
            if ($signalement->getUserSignalement() === $this) {
                $signalement->setUserSignalement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Signalement[]
     */
    public function getSignaled(): Collection
    {
        return $this->signaled;
    }

    public function addSignaled(Signalement $signaled): self
    {
        if (!$this->signaled->contains($signaled)) {
            $this->signaled[] = $signaled;
            $signaled->setUserSignaledd($this);
        }

        return $this;
    }

    public function removeSignaled(Signalement $signaled): self
    {
        if ($this->signaled->contains($signaled)) {
            $this->signaled->removeElement($signaled);
            // set the owning side to null (unless already changed)
            if ($signaled->getUserSignaledd() === $this) {
                $signaled->setUserSignaledd(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setOrganizer($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getOrganizer() === $this) {
                $event->setOrganizer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventRegistration[]
     */
    public function getEventRegistrations(): Collection
    {
        return $this->eventRegistrations;
    }

    public function addEventRegistration(EventRegistration $eventRegistration): self
    {
        if (!$this->eventRegistrations->contains($eventRegistration)) {
            $this->eventRegistrations[] = $eventRegistration;
            $eventRegistration->setParticipant($this);
        }

        return $this;
    }

    public function removeEventRegistration(EventRegistration $eventRegistration): self
    {
        if ($this->eventRegistrations->contains($eventRegistration)) {
            $this->eventRegistrations->removeElement($eventRegistration);
            // set the owning side to null (unless already changed)
            if ($eventRegistration->getParticipant() === $this) {
                $eventRegistration->setParticipant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluationsOthers(): Collection
    {
        return $this->evaluationsOthers;
    }

    public function addEvaluationsOther(Evaluation $evaluationsOther): self
    {
        if (!$this->evaluationsOthers->contains($evaluationsOther)) {
            $this->evaluationsOthers[] = $evaluationsOther;
            $evaluationsOther->setEvaluator($this);
        }

        return $this;
    }

    public function removeEvaluationsOther(Evaluation $evaluationsOther): self
    {
        if ($this->evaluationsOthers->contains($evaluationsOther)) {
            $this->evaluationsOthers->removeElement($evaluationsOther);
            // set the owning side to null (unless already changed)
            if ($evaluationsOther->getEvaluator() === $this) {
                $evaluationsOther->setEvaluator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Badge[]
     */
    public function getBadges(): Collection
    {
        return $this->badges;
    }

    public function addBadge(Badge $badge): self
    {
        if (!$this->badges->contains($badge)) {
            $this->badges[] = $badge;
        }

        return $this;
    }

    public function removeBadge(Badge $badge): self
    {
        if ($this->badges->contains($badge)) {
            $this->badges->removeElement($badge);
        }

        return $this;
    }

    public function getCertification(): ?Certification
    {
        return $this->certification;
    }

    public function setCertification(?Certification $certification): self
    {
        $this->certification = $certification;

        return $this;
    }

    public function getOwner(): ?self
    {
        return $this->owner;
    }

    public function setOwner(?self $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Block[]
     */
    public function getBlockeds(): Collection
    {
        return $this->blockeds;
    }

    public function addBlocked(Block $blocked): self
    {
        if (!$this->blockeds->contains($blocked)) {
            $this->blockeds[] = $blocked;
            $blocked->setBloked($this);
        }

        return $this;
    }

    public function removeBlocked(Block $blocked): self
    {
        if ($this->blockeds->removeElement($blocked)) {
            // set the owning side to null (unless already changed)
            if ($blocked->getBloked() === $this) {
                $blocked->setBloked(null);
            }
        }

        return $this;
    }
}
