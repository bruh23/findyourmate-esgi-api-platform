<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BlockRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BlockRepository::class)
 * @ApiResource(
 *
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "normalization_context"={"groups"={"block_post_norm","block_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          }
 *     },
 *
 *     itemOperations={
 *          "get",
 *     }
 * )
 */
class Block
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"block_post_norm"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"block_post_norm","block_post_denorm"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="blocks")
     * @ORM\JoinColumn(nullable=false )
     * @Groups({"block_post_denorm"})
     */
    private $user_blocker;


    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="blockeds")
     * @Groups({"block_post_denorm"})
     */
    private $user_blocked;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserBlocker(): ?User
    {
        return $this->user_blocker;
    }

    public function setUserBlocker(?User $user_blocker): self
    {
        $this->user_blocker = $user_blocker;

        return $this;
    }

    public function getUserBlocked(): ?User
    {
        return $this->user_blocked;
    }

    public function setUserBlocked(?User $user_blocked): self
    {
        $this->user_blocked = $user_blocked;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }



}
