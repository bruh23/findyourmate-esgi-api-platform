<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CertificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CertificationRepository::class)
 *
 * @ApiResource(
 *
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"certi_get_norm","certi_post_denorm"}},
 *              "security"="is_granted('ROLE_ADMIN')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *          "post"={
 *              "normalization_context"={"groups"={"certi_post_norm","certi_post_denorm"}},
 *              "denormalization_context"={"groups"={"certi_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          }
 *     },
 *
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"certi_get_norm","certi_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *          "patch"={
 *              "normalization_context"={"groups"={"certi_get_norm","certi_post_denorm"}},
 *              "denormalization_context"={"groups"={"certi_post_denorm","certi_patch_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          }
 *     }
 * )
 */
class Certification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user_get_no","certi_get_norm"})
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"user_get_no","certi_post_norm","certi_get_norm"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"user_get_no","certi_get_norm"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user_get_no","certi_get_norm","certi_patch_denorm"})
     */
    private $validation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_get_no","certi_get_norm","certi_patch_denorm"})
     */
    private $validatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user_get_no"})
     * @Groups({"certi_post_denorm","certi_post_norm","certi_patch_denorm"})
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="certification", cascade={"persist", "remove"})
     * @Groups({"certi_post_denorm","certi_post_norm"})
     */
    private $userCertification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getValidation(): ?bool
    {
        return $this->validation;
    }

    public function setValidation(bool $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getValidatedAt(): ?\DateTimeInterface
    {
        return $this->validatedAt;
    }

    public function setValidatedAt(\DateTimeInterface $validatedAt): self
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUserCertification(): ?User
    {
        return $this->userCertification;
    }

    public function setUserCertification(?User $userCertification): self
    {
        $this->userCertification = $userCertification;

        // set (or unset) the owning side of the relation if necessary
        $newCertification = null === $userCertification ? null : $this;
        if ($userCertification->getCertification() !== $newCertification) {
            $userCertification->setCertification($newCertification);
        }

        return $this;
    }


}
