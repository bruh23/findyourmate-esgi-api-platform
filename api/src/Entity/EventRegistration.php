<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EventRegistrationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EventRegistrationRepository::class)
 * @ApiResource(
 *  collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"event_reg_post_norm","event_reg_post_denorm","event_reg_get_norm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *           "post"={
 *              "normalization_context"={"groups"={"event_reg_post_norm","event_reg_post_denorm"}},
 *              "denormalization_context"={"groups"={"event_reg_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *     },
 *
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"event_reg_post_norm","event_reg_post_denorm","event_reg_get_norm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getParticipant().getOwner() == user)","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire / vous n'etes le owner "
 *          },
 *          "patch"={
 *              "normalization_context"={"groups"={"event_reg_patch_norm","event_reg_patch_denorm","event_reg_get_norm"}},
 *              "denormalization_context"={"groups"={"event_reg_patch_denorm"}},
 *              "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getEvent().getOrganizer().getOwner() == user)","security_message"="Voun n'etes pas l'organisateur de l'event! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *     }
 *     )
 */
class EventRegistration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"event_read_norm","event_reg_get_norm","user_get_no"})
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"event_read_norm","event_reg_post_norm","event_reg_patch_norm","user_get_no"})
     */
    private $createdAt;
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"event_reg_get_norm","event_reg_patch_norm","user_get_no"})
     */
    private $updatedAt;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"event_read_norm","event_reg_get_norm","event_reg_patch_denorm","user_get_no"})
     */
    private $validation;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"event_read_norm","event_reg_get_norm","user_get_no"})
     */
    private $validatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="eventRegistrations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"event_reg_post_denorm","event_reg_patch_norm","user_get_no"})
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eventRegistrations")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Groups({"event_read_norm","event_reg_post_denorm","event_reg_patch_norm"})
     */
    private $participant;

    /**
     * @ORM\OneToMany(targetEntity=Evaluation::class, mappedBy="eventRegistration", cascade={"persist", "remove"})
     * @Groups({"event_read_norm","event_reg_get_norm"})
     */
    private $evaluations;


    public function __construct()
    {
        $this->evaluations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getValidation(): ?bool
    {
        return $this->validation;
    }

    public function setValidation(?bool $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getValidatedAt(): ?\DateTimeInterface
    {
        return $this->validatedAt;
    }

    public function setValidatedAt(?\DateTimeInterface $validatedAt): self
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setEventRegistration($this);
        }

        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->contains($evaluation)) {
            $this->evaluations->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getEventRegistration() === $this) {
                $evaluation->setEventRegistration(null);
            }
        }

        return $this;
    }

    public function getParticipant(): ?User
    {
        return $this->participant;
    }

    public function setParticipant(?User $participant): self
    {
        $this->participant = $participant;

        return $this;
    }
}
