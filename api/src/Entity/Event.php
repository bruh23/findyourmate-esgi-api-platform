<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ApiResource(
 *
 *  attributes={
 *     "order"={"date": "DESC"}},
 *
 *
 *  collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"event_read_norm","event_read_norm_item"}},
 *             "security"="is_granted('ROLE_USER')", "security_message"="Ronaldo ,Sorry, but you are not the event owner."
 *           },
 *          "events": {
 *             "method": "get",
 *             "path": "/events",
 *             "controller": App\Controller\EventGetAllController::class,
 *              "normalization_context"={"groups"={"event_read_norm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          },
 *          "post"={
 *              "normalization_context"={"groups"={"event_post_norm","event_post_denorm"}},
 *              "denormalization_context"={"groups"={"event_post_denorm"}},
 *              "security"="is_granted('ROLE_USER')","security_message"="Erreur ! Une connexion à votre compte FindYourMate est obligatoire"
 *          }
 *
 *     },
 *
 *  itemOperations={
 *
 *          "get"={
 *              "normalization_context"={"groups"={"event_read_norm","event_read_norm_item"}},
 *             "security"="is_granted('ROLE_USER')", "security_message"="Ronaldo ,Sorry, but you are not the event owner."
 *           },
 *
 *          "delete"={
 *             "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getOrganizer().getOwner() == user)", "security_message"="Ronaldo ,Sorry, but you are not the event owner."
 *          },
 *
 *          "patch"={
 *              "normalization_context"={"groups"={"event_read_norm","event_put_norm"}},
 *              "denormalization_context"={"groups"={"event_put_denorm"}},
 *              "security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getOrganizer().getOwner() == user)", "security_message"="Ronaldo ,Sorry, but you are not the event owner."
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"category":"exact","category.name": "partial", "title": "ipartial","description": "ipartial"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"event_read_norm","user_get_no"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $nbParticipant;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $additionnalAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="datetimetz")
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $date;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetimetz")
     * @Groups({"event_post_norm","user_get_no"})
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"event_put_norm","user_get_no"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"event_read_norm","user_get_no_1"})
     */
    private $organizer;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"event_read_norm","event_post_denorm","event_put_denorm","user_get_no"})
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity=Chat::class, inversedBy="event", cascade={"persist", "remove"})
     * @Groups({"event_read_norm"})
     */
    private $chat;

    /**
     * @ORM\OneToMany(targetEntity=EventRegistration::class, mappedBy="event", cascade={"persist", "remove"})
     * @Groups({"event_read_norm"})
     */
    private $eventRegistrations;



    public function __construct()
    {
        $this->eventRegistrations = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbParticipant()
    {
        return $this->nbParticipant;
    }

    /**
     * @param mixed $nbParticipant
     */
    public function setNbParticipant($nbParticipant): void
    {
        $this->nbParticipant = $nbParticipant;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAdditionnalAddress(): ?string
    {
        return $this->additionnalAddress;
    }

    public function setAdditionnalAddress(?string $additionnalAddress): self
    {
        $this->additionnalAddress = $additionnalAddress;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getChat(): ?Chat
    {
        return $this->chat;
    }

    public function setChat(?Chat $chat): self
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * @return Collection|EventRegistration[]
     */
    public function getEventRegistrations(): Collection
    {
        return $this->eventRegistrations;
    }

    public function addEventRegistration(EventRegistration $eventRegistration): self
    {
        if (!$this->eventRegistrations->contains($eventRegistration)) {
            $this->eventRegistrations[] = $eventRegistration;
            $eventRegistration->setEvent($this);
        }

        return $this;
    }

    public function removeEventRegistration(EventRegistration $eventRegistration): self
    {
        if ($this->eventRegistrations->contains($eventRegistration)) {
            $this->eventRegistrations->removeElement($eventRegistration);
            // set the owning side to null (unless already changed)
            if ($eventRegistration->getEvent() === $this) {
                $eventRegistration->setEvent(null);
            }
        }

        return $this;
    }

    public function getOrganizer(): ?User
    {
        return $this->organizer;
    }

    public function setOrganizer(?User $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }
}
